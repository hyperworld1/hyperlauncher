pub const SUPPORTED_SERVER_API_VERSION: u32 = 1;

// Filesystem

#[cfg(windows)]
pub const DOWNLOAD_FILE: &str = "hyperworld.zip";
#[cfg(unix)]
pub const DOWNLOAD_FILE: &str = "hyperworld";

#[cfg(windows)]
pub const VOXYGEN_FILE: &str = "hyperworld-voxygen.exe";
#[cfg(unix)]
pub const VOXYGEN_FILE: &str = "hyperworld-voxygen";

#[cfg(windows)]
pub const LOGS_DIR: &str = "userdata\\voxygen\\logs";

#[cfg(unix)]
pub const LOGS_DIR: &str = "userdata/voxygen/logs";

#[cfg(unix)]
pub const SERVER_CLI_FILE: &str = "hyperworld-server-cli";

pub const SAVED_STATE_FILE: &str = "hyperlauncher_state.ron";
pub const LOG_FILE: &str = "hyperlauncher.log";

// Networking

// For querying
pub const CHANGELOG_URL: &str =
    "https://gitlab.com/hyperworld1/hyperworld/raw/{tag}/CHANGELOG.md";
// For user linking
pub const NEWS_URL: &str = "https://hyperealm.net/rss.xml";

pub const COMMUNITY_SHOWCASE_URL: &str = "https://hyperealm.net/community-showcase/rss.xml";

pub const GITLAB_MERGED_MR_URL: &str =
    "https://gitlab.com/hyperworld1/hyperworld/-/merge_requests?scope=all&sort=merged_at_desc&state=merged";

pub const AIRSHIPPER_RELEASE_URL: &str = "https://gitlab.com/hyperworld1/hyperlauncher/-/releases";

pub const OFFICIAL_AUTH_SERVER: &str = "https://auth.hyperealm.net";
pub const OFFICIAL_SERVER_LIST: &str = "https://serverlist.hyperealm.net";

pub const GITLAB_SERVER_BROWSER_URL: &str =
    "https://gitlab.com/hyperworld1/hyperserverbrowser#veloren-server-browser";

pub const HYPERWORLD_DOWNLOAD_URL: &str = "https://download.hyperchaos.net";
pub const HYPERWORLD_TEST_URL: &str = "http://192.168.3.9:8000";
pub const HYPERWORLD_STAGING_URL: &str = "http://192.168.3.8:8000";