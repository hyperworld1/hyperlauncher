#![allow(dead_code)]
use crate::gui::HyperlauncherTheme;

pub type Renderer = iced::Renderer<HyperlauncherTheme>;

pub type Element<'a, Message> =
    iced::Element<'a, Message, iced::Renderer<HyperlauncherTheme>>;
pub type Container<'a, Message> =
    iced::widget::Container<'a, Message, iced::Renderer<HyperlauncherTheme>>;
pub type Button<'a, Message> =
    iced::widget::Button<'a, Message, iced::Renderer<HyperlauncherTheme>>;
pub type ProgressBar = iced::widget::ProgressBar<iced::Renderer<HyperlauncherTheme>>;
pub type PickList<'a, T, Message> =
    iced::widget::PickList<'a, T, Message, iced::Renderer<HyperlauncherTheme>>;
pub type TextInput<'a, Message> =
    iced::widget::TextInput<'a, Message, iced::Renderer<HyperlauncherTheme>>;
pub type Rule = iced::widget::Rule<iced::Renderer<HyperlauncherTheme>>;
pub type Text<'a> = iced::widget::Text<'a, iced::Renderer<HyperlauncherTheme>>;
pub type Tooltip<'a, Message> =
    iced::widget::Tooltip<'a, Message, iced::Renderer<HyperlauncherTheme>>;
