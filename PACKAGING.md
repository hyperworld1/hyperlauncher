# Packaging Hyperlauncher

For *binary* packages the following urls can be used (replace `v*.*.*` with the latest version):

Windows: `https://github.com/hyperworld/hyperlauncher/releases/v*.*.*/download/hyperlauncher-windows.msi`
MacOS: `https://github.com/hyperworld/hyperlauncher/releases/v*.*.*/download/hyperlauncher-macos.tar.gz`
Linux: `https://github.com/hyperworld/hyperlauncher/releases/v*.*.*/download/hyperlauncher-linux.tar.gz`

Replace `v*.*.*` with `latest` to always get the latest release.

For *source* packages **do not** use the `master` branch. Always package latest release either via tag (`v*.*.*`) or branch (`r*.*`) as master is unstable and contains work in progress features.