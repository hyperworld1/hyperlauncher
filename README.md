# Hyperlauncher

[![GitHub release)](https://img.shields.io/github/v/release/hyperworld/hyperlauncher?include_prereleases)](https://github.com/hyperworld/Hyperlauncher/releases) [![License](https://img.shields.io/github/license/hyperworld/hyperlauncher?color=blue)](https://github.com/hyperworld/Hyperlauncher/blob/master/LICENSE) [![Discord](https://img.shields.io/discord/449602562165833758?label=discord)](https://discord.gg/rvbW3Z4) [![AUR version](https://img.shields.io/aur/version/hyperlauncher?label=AUR)](https://aur.archlinux.org/packages/hyperlauncher/)

A cross-platform Hyperworld launcher.

![Hyperlauncher](https://i.imgur.com/1VkndRZ.gif)

## Features

- [x] Update/Download and start nightly.
- [x] Fancy UI with batteries included.
- [x] Updates itself on windows.

## Download

**NOTE:** Hyperlauncher cannot be considered stable yet.

#### Compile from source

```bash
git clone https://gitlab.com/hyperworld/hyperlauncher.git
cd hyperlauncher
cargo run --release
```

Make sure to have [rustup](https://rustup.rs/) installed to compile rust code and [git lfs](https://book.hyperworld.net/contributors/development-tools.html#git-lfs) for assets.

### Hyperlauncher-Server

**NOTE:** Hyperlauncher-Server is not required by end-users.

#### Compile from source

```bash
git clone https://gitlab.com/hyperworld/hyperlauncher.git
cd hyperlauncher
cargo run --release --bin hyperlauncher-server
```

On first execution, a template configuration file will be created at `config/config.template.ron` and the server will exit.

Rename this to `config.ron` and edit as appropriate before running again.

```bash
cargo run --release --bin hyperlauncher-server
```

#### For NixOS users

You can install Hyperlauncher with:
- Flakes enabled Nix: `nix profile install github:hyperworld/Hyperlauncher`
- Flakes disabled Nix: `nix-env -i -f "https://github.com/hyperworld/Hyperlauncher/tarball/master"`
